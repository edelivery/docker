# Docker

## Getting started

Before you start please make sure you haveinstalled the latest versions of the [Docker Engine](https://docs.docker.com/engine) (at the time of writing, the current version is v24.0.6) or the [Docker Desktop](https://docs.docker.com/desktop) (at the time of 
writing, the current version is 4.25.1).

The images were tested on Linux Ubuntu 22.04 with docker: Docker Engine v24.0.6.
To start 
- [ ] install docker
- [ ] download the appropiate docker compose example for your setup(single-instance or cluster):
    - [Single-instance: docker-compose.yml](https://code.europa.eu/edelivery/docker/-/blob/main/src/single-instance/docker-compose.yml)
    - [Cluster: docker-compose.yml](https://code.europa.eu/edelivery/docker/-/blob/main/src/cluster/docker-compose_tomcat.yml)
- [ ] start the environment

## Usage

* Start:    docker-compose up -d
* Shutdown: docker-compose down
* Logs:     docker-compose logs


## Configuration

This file represents the architecture of the Domibus ActiveMQ Shared File System Master Slave and Tomcat Cluster
MySQL (Corner 2) and Wildfly Cluster MySQL (Corner 3) test environment:
*    Corner 2 (C2)
- activemqi1: External ActiveMQ node 1 (enters Master mode if first to acquire lock to the KahaDB on the shared FS; otherwise, enters Slave move)
- activemqi2: External ActiveMQ node 2 (enters Master mode if first to acquire lock to the KahaDB on the shared FS; otherwise, enters Slave move)
- mysql:    MySQL Database with Domibus schema
- tomcati1: Tomcat Server (with domibus.war deployed)
- tomcati2: Tomcat Server (with domibus.war deployed)
- httpd:    Apache HTTPD Load Balancer

## Images

Please use the links below in order to acces the documentation for each image:

[Domibus Tomcat Image](https://code.europa.eu/edelivery/docker/-/tree/main/src/tomcat)

[Domibus ActiveMQ Image](https://code.europa.eu/edelivery/docker/-/tree/main/src/activemq)

[Edelivery HTTPD Image](https://code.europa.eu/edelivery/docker/-/tree/main/src/httpd)

[Domibus MySQL Image](https://code.europa.eu/edelivery/docker/-/tree/main/src/mysql)

## Guides

For setting up Domibus as a single instance or as a clustered environment using Docker please check the guides below:

[Single Instance](https://code.europa.eu/edelivery/docker/-/tree/main/src/single-instance)

[Cluster](https://code.europa.eu/edelivery/docker/-/tree/main/src/cluster)

## Upgrades

For upgrading the currently used images please check the below guides:

[Upgrade the Tomcat image](https://code.europa.eu/edelivery/docker/-/blob/main/src/tomcat/upgrade/README.md)

[Upgrade the ActiveMQ image](https://code.europa.eu/edelivery/docker/-/tree/main/src/activemq/upgrade)
