# Guide on setting up a Domibus single instance with Docker

## Getting started

This is a step-by-step guide which will help you setup an Domibus instance using Docker and exchange
messages with the eDelivery Domibus Access Point. These instructions are meant for a setup in a test environment. For a production ready installation please follow the recommendations from the Domibus Administration Guide.

During this guide we will follow the steps:
- [ ] Set up a Domibus single instance on your machine using Docker
- [ ] Configure Domibus to exchange AS4 messages with the eDelivery Domibus Access Point and the other webinar participants
- [ ] Exchange AS4 messages with the eDelivery Domibus Access Point using SoapUI and the Web Service Plugin
- [ ] Exchange AS4 messages between the webinar participants

Please note that the "x" from the image name is the Domibus release version(e.g 5.0.7, 5.1.1 etc.) and "y" is used in case the image contains a Docker fix. If there is no Docker fix released after the Domibus release, the "-y" ending will not be present in the image name.

## Prerequisites

Before executing the above steps, please make sure you that you comply
with the below prerequisites:

- [ ] Install SoapUI
- [ ] Install Docker engine including docker compose
- [ ] You have a public IP with firewall rules allowing a port reachable from the internet
- [ ] Docker images already pulled
- [ ] The package containing the PKI certificates, PMode file and the SoapUI project.


## Steps

### Step 1 - Download Docker images

- `docker pull code.europa.eu:4567/edelivery/docker/domibus-tomcat9:x-y`
- `docker pull code.europa.eu:4567/edelivery/docker/domibus-mysql8:x-y`

### Step 2 - Download docker-compose.yml file

- Download the following Docker compose file into a new directory. We will refer to this directory as `DOMIBUS_DOCKER`:


```shell
https://code.europa.eu/edelivery/docker/-/raw/main/src/single-instance/docker-compose.yml
```

On a Linux system you can use in the command line:
```shell
wget https://code.europa.eu/edelivery/docker/-/raw/main/src/single-instance/docker-compose.yml
```


> **NOTE** Make sure that it’s saved under the name docker-compose.yml

- Open the `docker-compose.yml` file and locate the following:

```yaml
ports:
- "18080:8080"
```

- Change the `18080` port to the port that you have opened to the internet. We will refer to the port opened to the internet as `YOUR_PORT`.

> **NOTE** If you use a reverse proxy, you need to configure the reverse proxy to forward to the internal port

### Step 3 - Start Domibus

- Execute the following commands in a terminal from inside the `DOMIBUS_DOCKER` directory:

```shell
docker-compose up -d
```

```shell
docker-compose logs -f
```

> **NOTE** Please be patient, it can take between 2-10 minutes to start

- When you see the following line in the logs, it means that Domibus started successfully:

```shell
org.apache.catalina.startup.Catalina.start Server startup in [52508] milliseconds
```

### Step 4 - Verify Domibus configuration

- Check that a folder named `domibus` is created in the `DOMIBUS_DOCKER` directory which contains the Domibus configuration files

- Please make sure your user has write permissions to the `domibus` folder created above

### Step 5 - Login into the Domibus Admin Console

- Open a browser and open the following URL:

`http://YOUR_DOMIBUS_URL:YOUR PORT/domibus`

- Login in the Domibus Admin Console with the credentials:

`admin/123456`


### Step 6 - Upload the PMode file

- Navigate to PMode->Current page in the Domibus Admin Console
- Upload the PMode file you have received from the eDelivery team

> **NOTE** For using this Access Point in a specific network, please replace the PMode file with one provided by the network

### Step 7 - Configure Domibus keystore

- Execute the following in a terminal from the DOMIBUS_DOCKER directory:

```shell
docker-compose stop
```

- Override the existing keystore `DOMIBUS_DOCKER/domibus/keystores/gateway_keystore.jks`
  with the `gateway_keystore.jks` file you have received from the eDelivery team

- Modify the property `domibus.security.key.private.alias` from the `DOMIBUS_DOCKER/domibus/domibus.properties` file to set it to your party id
  Eg: `domibus.security.key.private.alias=be_test_ap`


### Step 8 - Configure Domibus truststore

- Execute the following in a terminal from the DOMIBUS_DOCKER directory:

```shell
docker-compose start
```

```shell
docker-compose logs -f
```

- Wait for Domibus to start up
- Login into the Domibus Admin Console:

URL: `http://YOUR_DOMIBUS_URL:YOUR_PORT/domibus`

Credentials: `admin/123456`

- Navigate to `Truststores->Domibus`
- Upload the truststore you have received from the eDelivery team. The password is: `test123`
- Press the button `Reload KeyStore` in the lower right corner. You should see the message `Keystore was successfully reset`

> **NOTE** For using this Access Point in a specific network, please replace the truststore with one provided by the network

### Step 9 - Send a message to the eDelivery Domibus Access Point

- Open SoapUI
- Import the SoapUI project you have received from the eDelivery team:
    - File->Import Project
- Open the Soap UI request    
- Submit request

### Step 10 - Exchange messages between webinar participants

- Exchange messages between the participants

## Docker images

You can find [here](https://code.europa.eu/edelivery/docker) the documentation for the Docker images used in this guide.

## Support and contact information

Have questions? Consult our [Q&A section]( https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus+FAQs).
Or ask your questions using [stackoverflow](http://stackoverflow.com/questions/ask).
Please use the tag `context.domibus`.

Still have questions? Contact [eDelivery support](https://ec.europa.eu/digital-building-blocks/tracker/plugins/servlet/desk/portal/6),
or by email: EC-EDELIVERY-SUPPORT@ec.europa.eu
