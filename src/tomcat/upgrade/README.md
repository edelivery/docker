## Upgrade the Domibus Tomcat Image

In order to update the domibus-tomcat image from an older version to 5.1.3, the next steps must be followed:

- retrieve the new version from the repo: docker pull code.europa.eu:4567/edelivery/docker/domibus-tomcat:5.1.3
- update the image version in the docker-compose setup file where the image is used:
```
version: '3.2'
services:
  tomcatc2:
      image: domibus-tomcat9:5.1.3
      environment:
         - DB_TYPE=MySQL
         - DB_HOST=mysqlc2
         - DB_PORT=3306
         - DB_USER=edelivery
         - DB_PASS=edelivery
         - DOMAINS_COUNT=1
         - DOMAIN_ALIASES=blue_gw
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus
      depends_on:
        - mysqlc2
```
- in case the environment variables list has been changed(please check the documentation of the new released image), it needs to be updated in the above configuration

- follow the instructions from the Domibus upgrade-info.md file: https://ec.europa.eu/digital-building-blocks/code/projects/EDELIVERY/repos/domibus/browse/Core/Domibus-MSH/upgrade-info.md?at=refs%2Fheads%2F5.1.3 for the configuration updates and database migration. Follow all the steps from the old installed version up to the desired new version(in this case 5.1.3)

- upgrade the config files in your project setup folder: ./c2/conf/domibus accordingly. Do the same for the c3 corner.

- stop the environment by running the following comand(with the appropriate parameters): 

      docker compose down

- start the environment again(with the appropriate parameters): 

      docker compose up
