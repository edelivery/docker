## Disclaimer

This image is not meant to be used in production (is for testing only). Users have the responsibility to address the vulnerabilities related to this Docker image.

# eDelivery HTTPD

This image is created from httpd:2.4.58.
It contains an HTTPD server prepared to be used as a load balancer on a Domibus clustered environment.

This image is provided for testing only, not for production usage.

### Usage

Usage via docker-compose, e.g:

```
version: '3.2'
services:
    httpdc2:
    depends_on:
      - tomcatc2i1
      - tomcatc2i2
    image: edelivery-httpd:2.4.58
    environment:
      - VHOST_CORNER_HOSTNAME=edelivery.domibus.eu
      - NODES_COUNT=2
      - NODE_HOSTNAMES=tomcatc2i1,tomcatc2i2
      - NODE_PORT_NUMBERS=8080,8080
    hostname: httpdc2
    ports:
      - "8082:80"
```
   
### Vulnerabilities check

Please note that the edelivery-httpd:x-y image provided is based on the latest httpd official image, available at the time when this documentation is written, httpd:2.4.58 (from https://hub.docker.com/_/httpd/), which might have security vulnerabilities that are independent of Domibus. It is highly recommended that you scan the image for vulnerabilities and decide if they affect you before using it.

### More information

Please check the base image:
https://hub.docker.com/_/httpd/
