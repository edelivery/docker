## Upgrade the Domibus ActiveMQ Image

In order to update the domibus-activemq image from an older version to 5.1.3, the next steps must be followed:

- retrieve the new version from the repo: docker pull code.europa.eu:4567/edelivery/docker/domibus-activemq:5.1.3
- update the image version in the docker-compose setup file where the image is used:
```
version: '3.2'
services:
  activemqc2:
    image: domibus-activemq:5.1.3
    environment:
      - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker
      - ACTIVEMQ_WEB_CONSOLE_PORT=8161
    volumes:
      - ./activemq-db:/usr/local/activemq/work/kahadb
      - ./c2/conf/activemq:/usr/local/activemq/conf
  # ports:
  #   - "8161:8161"
```
- in case the environment variables list has been changed(please check the documentation of the new released image), it needs to be updated above

- follow the instructions related to ActiveMQ from the Domibus upgrade-info.md file: https://ec.europa.eu/digital-building-blocks/code/projects/EDELIVERY/repos/domibus/browse/Core/Domibus-MSH/upgrade-info.md?at=refs%2Fheads%2F5.1.3 for the configuration updates. Follow all the steps related to ActiveMQ from the old installed version up to the desired new version(in this case 5.1.3)

- upgrade the config files in your project setup folder: ./c2/conf/activemq accordingly. Do the same for the c3 corner.

- stop the environment by running the following comand(with the appropriate parameters): 

      docker compose down

- start the environment again(with the appropriate parameters): 

      docker compose up
