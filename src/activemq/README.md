## Disclaimer

Users have the responsibility to address the vulnerabilities related to this Docker image.

## Domibus ActiveMQ Image

The domibus-activemq:x-y contains an ActiveMQ broker instance. It is used to run an
external ActiveMQ broker that can act as standalone or be part of a master-slave cluster. Please note that the "x" from the image name is the Domibus release version(e.g 5.0.8, 5.1.2 etc.) and "y" is used for the Docker image eg in case of fixes related to the Docker image itself. If there is no Docker fix released after the Domibus release, the "-y" ending will not be present in the image name.

In the docker compose examples below, bind mounts(eg ./activemq-db, ./c2/conf/activemq) are used when defining the volumes. Instead of using bind mounts, the user can also define a shared file system and use that one instead. If using bind mounts, care has to be taken not to create them with the root user.

### Environment Properties

| Name	                             | Description                                                                                                          |                          Required                           |                     Default |
|-----------------------------------|----------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------:|----------------------------:|
| ACTIVEMQ_DIR                      | The folder where ActiveMQ is installed                                                                               |                                                             |         /usr/local/activemq |
| ACTIVEMQ_INSTALL_DIR              | The folder where the ActiveMQ installation and configuration files are copied to before installation                 |                                                             | /usr/local/activemq/install |
| ACTIVEMQ_VERSION                  | The ActiveMQ version that will be used for the installation. It is passed in with the ${ACTIVEMQ_VERSION} argument   |                                                             |                       6.1.1 |
| ACTIVEMQ_HOST                     | The hostname or the IP address used to build the bind address for the TCP transport of the ActiveMQ external broker. |                                                             |                     0.0.0.0 |
| ACTIVEMQ_JMX_ACTIVE                     | Flag to enable/disable the JMX usage. |                                                             |                     true |
| ACTIVEMQ_BROKER_NAME              | The name of the ActiveMQ external broker                                                                             |                                                             |       domibusActiveMQBroker |
| ACTIVEMQ_CONNECTOR_PORT           | The port number for the JMX connector of the ActiveMQ external broker                                                |                                                             |                        1199 |
| ACTIVEMQ_TRANSPORT_CONNECTOR_PORT | The port number for the transport connector URI of the ActiveMQ external broker                                      |                                                             |                       61616 |
| ACTIVEMQ_WEB_CONSOLE_PORT         | The port which will be used for the WebConsole, the ActiveMQ management console                                      |                                                             |                        8161 |
| ACTIVEMQ_WEB_CONSOLE_ENABLED      | Flag to enable/disable the WebConsole                                                                                |                                                             |                        true |
| ACTIVEMQ_DATA                     | ActiveMQ data folder                                                                                                 |                                                             |    /usr/local/activemq/data |
| ACTIVEMQ_TMP                      | ActiveMQ temp folder                                                                                                 |                                                             |     /usr/local/activemq/tmp |
| QUEUE_FS_PLUGIN_SEND              | FS Plugin send queue                                                                                                 |                                                             | domibus.fsplugin.send.queue |
| QUEUE_WS_PLUGIN_SEND              | WS Plugin send queue                                                                                                 |                                                             | domibus.wsplugin.send.queue |
| TZ                                | Timezone                                                                                                             |                                                             |             Europe/Brussels |

### Standalone

Usage via docker-compose:
```
version: '3.2'
services:
  activemqc2:
    image: domibus-activemq:x-y
    environment:
      - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker
      - ACTIVEMQ_WEB_CONSOLE_PORT=8161
    volumes:
      - ./activemq-db:/usr/local/activemq/work/kahadb
      - ./c2/conf/activemq:/usr/local/activemq/conf
  # ports:
  #   - "8161:8161"
```
### Cluster
An external cluster of exactly 2 ActiveMQ independent broker instances. The broker instances have no knowledge of each other.

Usage via docker-compose:
```
version: '3.2'
services:
  activemqc2i1:
    image: domibus-activemq:x-y
    environment:
      - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker1
      -  ACTIVEMQ_WEB_CONSOLE_PORT=8161
    volumes:
      - ./c2/conf/activemq1:/usr/local/activemq/conf
   # ports:
   #   - "8161:8161"

  activemqc2i2:
    image: domibus-activemq:x-y
    environment:
      - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker2
      - ACTIVEMQ_WEB_CONSOLE_PORT=8162
    volumes:
      - ./c2/conf/activemq2:/usr/local/activemq/conf
    #ports:
   #   - "8162:8162"
```

### Master-Slave
An external cluster of ActiveMQ broker instances running in master-slave mode using a shared files system. The broker
instances share the same KahaDB via a volume mapped to the same host directory. This makes ActiveMQ nodes race for the
same KahaDB lock file, with the node which has the exclusive lock being the master and active node while the remaining
nodes acting as slaves in a failover system, in which when a node releases the lock for whatever reason it loses master
status and the node which grabs the lock becomes the new master.

Usage via docker-compose:
```
version: '3.2'
services:
  activemqc2i1:
    image: domibus-activemq:x-y
    environment:
      - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker1
      - ACTIVEMQ_WEB_CONSOLE_PORT=8161
    volumes:
      - ./activemq-db:/usr/local/activemq/work/kahadb
      - ./c2/conf/activemq1:/usr/local/activemq/conf
  # ports:
  #   - "8161:8161"

  activemqc2i2:
    image: domibus-activemq:x-y
    environment:
      - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker2
      - ACTIVEMQ_WEB_CONSOLE_PORT=8162
    volumes:
      - ./activemq-db:/usr/local/activemq/work/kahadb
      - ./c2/conf/activemq2:/usr/local/activemq/conf
  # ports:
  #   - "8162:8162"
```
### Configure users and user rights

It is possible to configure multiple users for the ActiveMQ instances. This can be achieved by overwriting the users.properties file from the following locations(according to the container that is being configured):
 - ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (and activemq2 respectively) for C2
 - ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (and activemq2 respectively) for C3

 These locations are then referenced in the ActiveMQ volumes section of the image definition inside the docker-compose.yml file, as in the below example for C2:
```
    volumes:
      - ./c2/conf/activemq2:/usr/local/activemq/conf
```
A container restart is needed for the changes to take effect (running of the 2_startup.sh script from the 'compose' folder).
A sample file is already copied, having contents similar to the one below:

users.properties:
```
#In order to define a user add a new line in the form: username=password

admin=123456
domibus=changeit
```

Updating the user rights, can be achieved by overwriting the groups.properties file from the locations specified above. Same as for defining the users, a container restart is needed for the changes to take effect (running of the 2_startup.sh script from the 'compose' folder).
A sample groups.properties file is already copied, having contents similar to the one below:

groups.properties:
```
#In order to assign rights to a specific user, add the user to one of the user groups

admins=admin
users=domibus
```

If the ActiveMQ containers are rebuilt(the 1_buildImages.sh script is executed), the users.properties and groups.properties files are overwritten with the sample ones and the user needs to update them again.

### Configure queues

The user has the possibility to change queue configurations by editing or overwriting the existing activemq.xml file from one of the locations below(according to the container that is being configured):
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (and activemq2 respectively) for C2
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (and activemq2 respectively) for C3

The user can configure certain settings like memory usage, store usage, authorizations, number of redeliveries, etc, however 
the user must not change other queue definitions except FSPlugin and WSPlugin, since the changes will not be supported by Domibus.
Changing the FSPlugin and WSPlugin queue names can be made by defining the QUEUE_FS_PLUGIN_SEND and QUEUE_WS_PLUGIN_SEND environment variables in the corresponding 
ActiveMQ service section from the compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml file. The default values are:
- QUEUE_FS_PLUGIN_SEND=domibus.fsplugin.send.queue
- QUEUE_WS_PLUGIN_SEND=domibus.wsplugin.send.queue

### Web console
In order to monitor the ActiveMq instance, a web console is available on https://HostIP:ACTIVEMQ_WEB_CONSOLE_PORT.
The default port is 8161, and it can be configured in compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml, in the section of the corresponding ActiveMQ service:
- ACTIVEMQ_WEB_CONSOLE_PORT=8161

It highly recommended not to expose these ports on the internet.

Please be aware that the connection to the web console is SSL secured and therefore the URL begins with https. 
The secured connection can be configured in the following section of jetty.xml:
```
<bean id="SecureConnector" class="org.eclipse.jetty.server.ssl.SslSelectChannelConnector">
    <property name="port" value="8161" />
    <property name="keystore" value="file:${activemq.conf}/broker.ks" />
    <property name="password" value="password" />
</bean>
```
jetty.xml can be edited or overwritten, and it is located in one of the following folders(please configure the one corresponding to the corresponding ActiveMQ instance):
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (or activemq2 respectively) for C2
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (or activemq2 respectively) for C3

The default keystore file is broker.ks, located in the folders above, it can be replaced/renamed/edited and configuration must be updated accordingly.  
Also it can be replaced by a .jks file.

The web console users are defined in the jetty-realm.properties file, which can be customized by updating or overwriting the file in the corresponding container location below(according to the container that is being configured):
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (or activemq2 respectively) for C2 
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (or activemq2 respectively) for C3

A sample jetty-realm.properties is shown below:
```
# Defines users that can access the web (console, demo, etc.)
# username=password [,rolename ...]
admin: admin, admin
user: user, user
```

It is possible to enable/disable the web console by setting the environment variable ACTIVEMQ_WEB_CONSOLE_ENABLED in corresponding ActiveMQ service section inside compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml:
- ACTIVEMQ_WEB_CONSOLE_ENABLED=true
By default it is enabled.


### Vulnerabilities check

Please note that the domibus-activemq:x-y image provided is based on almalinux:9 (from https://hub.docker.com/_/almalinux/) and contains activemq version 6.1.1, which may contain security vulnerabilities that are independent of Domibus. 
It is highly recommended that you scan the image for vulnerabilities and decide if they affect you before using it.
In case this image contains vulnerabilities that affect your organisation, please use the latest image from our Docker repository.
