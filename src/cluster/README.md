# Guide on setting up a Domibus cluster instance with Docker

## Getting started

This is a step-by-step guide which will help you setup a Domibus cluster instance using Docker and exchange
messages with the eDelivery Domibus Access Point. These instructions are meant for a setup in a test environment. For a production ready installation please follow the recommendations from the Domibus Administration Guide.

During this guide we will follow the steps:
- [ ] Set up a Domibus cluster instance in your environment
- [ ] Configure your Domibus to exchange AS4 messages with the eDelivery Domibus Access Point and the other webinar participants
- [ ] Exchange AS4 messages with the eDelivery Domibus Access Point using SoapUI and the Web Service Plugin
- [ ] Exchange AS4 messages between the webinar participants

Please note that the "x" from the image name is the Domibus release version(e.g 5.0.7, 5.1.1 etc.) and "y" is used in case the image contains a Docker fix. If there is no Docker fix released after the Domibus release, the "-y" ending will not be present in the image name.

## Prerequisites

Before executing the above steps, please make sure you that you comply
with the below prerequisites:

- [ ] Install SoapUI
- [ ] Install Docker engine including docker compose on the machines where Tomcat and, optionally, ActiveMQ Classic JMS broker will be installed
- [ ] If you use our Docker images to install the ActiveMQ Classic JMS broker, you need to have a shared filesystem (e.g. NAS) which will be used by the ActiveMQ Classic JMS broker instances. The ActiveMQ instances must have write permissions to the shared file system
- [ ] If you do not use our Docker images to install the ActiveMQ Classic JMS broker, you need to set up an instance
- [ ] You have a shared filesystem (e.g. NAS) which will be used by the Tomcat instances. The Tomcat instances must have write permissions to the shared file system
- [ ] The Tomcat instances must be able to reach the ActiveMQ Classic JMS broker. The following ports must be reachable:
  - JMX port, default value is 1199
  - Transport connector port, default value is 61616
- [ ] You have a MySQL 8 database instance which can be reached by the machines where Tomcat is installed. The default listening port for MySQL is 3306
- [ ] You have a load balancer which can be reached from the internet. The load balancer must be configured to route the incoming traffic to the machines where Tomcat is installed. The default listening port for Tomcat is 8080
- [ ] Docker images already pulled
- [ ] The package containing the PKI certificates, PMode file and the SoapUI project.


## Steps

### Step 1 - Download Docker images

- `docker pull code.europa.eu:4567/edelivery/docker/domibus-tomcat9:x-y`

Optionally, if you use our Docker images to install the ActiveMQ Classic JMS broker:
- `docker pull code.europa.eu:4567/edelivery/docker/domibus-activemq:x-y`

### Step 2 - Download docker-compose.yml files

- Download the following Docker compose template files for the Tomcat instances, each into a new directory on the machines that are part of your environment. We will refer to these directories as `DOMIBUS_TOMCAT`:

```shell
https://code.europa.eu/edelivery/docker/-/raw/main/src/cluster/docker-compose_tomcat.yml
```
On a Linux system you can use in the command lines:
```shell
wget https://code.europa.eu/edelivery/docker/-/raw/main/src/cluster/docker-compose_tomcat.yml
```
> **NOTE** Make sure docker-compose_tomcat.yml is saved under the name docker-compose.yml

If you plan to use our Docker images to install the ActiveMQ Classic JMS broker, download the following Docker compose template files for the ActiveMQ instances, each into a new directory on the machines that are part of your environment. We will refer to these directories as `DOMIBUS_ACTIVEMQ`:
```shell
https://code.europa.eu/edelivery/docker/-/raw/main/src/cluster/docker-compose_activemq.yml
```
On a Linux system you can use in the command lines:
```shell
wget https://code.europa.eu/edelivery/docker/-/raw/main/src/cluster/docker-compose_activemq.yml
```
> **NOTE** Make sure docker-compose_activemq.yml is saved under the name docker-compose.yml

### Step 3 - Start Domibus Cluster Environment

#### Step 3.1 Start MySQL 8 Database

The installation is left to your discretion, but you must follow the prescriptions of the section 4.1.1 from the Domibus v5.x Admin Guide.

> **NOTE** We will later refer to the hostname and the port where the database can be reached on the machines running the Tomcat instances as `YOUR_DATABASE_HOSTNAME` for the hostname and `YOUR_DATABASE_PORT` for the port.
> We will also refer to the name of your database as `YOUR_DATABASE_NAME` and to the credentials used to connect to it as `YOUR_DATABASE_USERNAME` and the `YOUR_DATABASE_PASSWORD`.

#### Step 3.2 Start ActiveMQ Classic JMS broker instances

The installation is left to your discretion, but you must follow the prescriptions of the section 4.3.3 from the Domibus v5.x Admin Guide.

> **NOTE** We will later refer to the hostnames where the ActiveMQ instances can be reached on the machines running the Tomcat instances as `YOUR_ACTIVEMQ1_HOSTNAME` for the first ActiveMQ instance, `YOUR_ACTIVEMQ2_HOSTNAME` for the second ActiveMQ instance and so on.

Alternatively, if you use our Docker images to install the ActiveMQ Classic JMS broker, open the `docker-compose.yml` file from inside the `DOMIBUS_ACTIVEMQ` directory and update the following sections:
1. broker name:
```yaml
    environment:
      - ACTIVEMQ_BROKER_NAME=YOUR_UNIQUE_BROKER_NAME
```

Change the `YOUR_UNIQUE_BROKER_NAME` value to the broker name.
> **NOTE** All the ActiveMQ instances must have unique broker names values defined in `YOUR_UNIQUE_BROKER_NAME`. For example, use something like `domibusActiveMQBroker1` for the first ActiveMQ instance, `domibusActiveMQBroker2` for the second ActiveMQ instance and so on.

> **NOTE** We will later refer to the broker name values on the machines running the Tomcat instances as follows:
> - `YOUR_ACTIVEMQ1_BROKER_NAME` for the first ActiveMQ instance, `YOUR_ACTIVEMQ2_BROKER_NAME` for the second ActiveMQ instance and so on.

2. credentials:

The credentials are configured in a users.properties file that should be copied in the ActiveMQ config location, for example: ./c2/conf/activemq1
A sample file can have the following content:

users.properties:
```
#In order to define a user add a new line in the form: username=password

admin=123456
domibus=changeit
```

The user rights can be configured using a groups.properties file, that can have a content similar to the one below:

groups.properties:
```
#In order to assign rights to a specific user, add the user to one of the user groups

admins=admin
users=domibus
```
For further details regarding this topic please check the section "Configure users and user rights" from the ActiveMQ image documentation: https://code.europa.eu/edelivery/docker/-/blob/main/src/activemq/README.md

3. shared file system:
```yaml
    volumes:
      - YOUR_ACTIVEMQ_SHARED_FILE_SYSTEM:/usr/local/activemq/work/kahadb
      - YOUR_ACTIVEMQ_SHARED_FILE_SYSTEM/YOUR_ACTIVEMQ_CORNER_CONFIG_PATH:/usr/local/activemq/conf
```

Change the `YOUR_ACTIVEMQ_SHARED_FILE_SYSTEM` value to the location of the ActiveMQ shared file system.
> **NOTE** All the ActiveMQ instances must use the same shared file system location value defined in `YOUR_ACTIVEMQ_SHARED_FILE_SYSTEM`.
The value `YOUR_ACTIVEMQ_CORNER_CONFIG_PATH` is a path relative to `YOUR_ACTIVEMQ_SHARED_FILE_SYSTEM`, for example: '/c2/conf/activemq1' for corner c2, which is the place where you add or overwrite the users.properties and groups.properties files.

4. port mappings:
```yaml
    ports:
      - "1199:1199"
      - "61616:61616"
```

Optionally, you can change the left side of each port mapping to an open port that is available on the host where your ActiveMQ instance is running.
> **NOTE** We will later refer the port mapping values on the machines running the Tomcat instances as follows:
> - the port mapping for `1199` as `YOUR_ACTIVEMQ1_PORT_1199` for the first ActiveMQ instance, `YOUR_ACTIVEMQ2_PORT_1199` for the second ActiveMQ instance and so on;
> - the port mapping for `61616` as `YOUR_ACTIVEMQ1_PORT_61616` for the first ActiveMQ instance, `YOUR_ACTIVEMQ2_PORT_61616` for the second ActiveMQ instance and so on.

- Execute the following commands in a terminal from inside the `DOMIBUS_ACTIVEMQ` directory on each ActiveMQ instance:
```shell
docker-compose up -d
```
```shell
docker-compose logs -f
```

The first ActiveMQ instance that will be able to acquire the lock on the shared file system will become the `Master`; all the other ActiveMQ instances become `Slave`s.
- When you see the following line in the logs, it means that the ActiveMQ started successfully, in `Master` mode:
```shell
sun.reflect.DelegatingMethodAccessorImpl.invoke Server startup in 418 ms
```

- When you see the following line in the logs, it means that the ActiveMQ started successfully, in `Slave` mode:
```shell
org.apache.activemq.store.SharedFileLocker.doStart Database /usr/local/activemq/work/kahadb/lock is locked by another server. This broker is now in slave mode waiting a lock to be acquired
```

#### Step 3.3 Tomcat instances

> **NOTE** We will later refer to the hostnames where the Tomcat instances can be reached on the machine running the load balancer as `YOUR_TOMCAT1_HOSTNAME` for the first Tomcat instance, `YOUR_TOMCAT2_HOSTNAME` for the second Tomcat instance and so on.

Open the `docker-compose.yml` file from inside the `DOMIBUS_TOMCAT` directory and update the following sections:
1. tomcat node:
```yaml
    environment:
      - TOMCAT_NODE=YOUR_UNIQUE_TOMCAT_NODE
```
Change the `YOUR_UNIQUE_TOMCAT_NODE` to the Tomcat node.

> **NOTE** All the Tomcat instances must have unique number values defined in `YOUR_UNIQUE_TOMCAT_NODE`. For example, use something like `01` for the first Tomcat instance, `02` for the second Tomcat instance and so on.

2. database:
```yaml
    environment:
      - DB_NAME=YOUR_DATABASE_NAME
      - DB_HOST=YOUR_DATABASE_HOSTNAME
      - DB_PORT=YOUR_DATABASE_PORT
      - DB_USER=YOUR_DATABASE_USERNAME
      - DB_PASS=YOUR_DATABASE_PASSWORD
```
Change the `YOUR_DATABASE_NAME`, `YOUR_DATABASE_HOSTNAME`, `YOUR_DATABASE_PORT`, `YOUR_DATABASE_USERNAME` and the `YOUR_DATABASE_PASSWORD` values using the ones specific to your MySQL 8 database, as you previously defined these in [Step 3.1](#step-31-start-mysql-8-database) above.

3. ActiveMQ:
```yaml
    environment:
      - ACTIVEMQ_USERNAME=YOUR_ACTIVEMQ_USERNAME
      - ACTIVEMQ_PASSWORD=YOUR_ACTIVEMQ_PASSWORD
      - ACTIVEMQ_BROKER_NAME=YOUR_ACTIVEMQ1_BROKER_NAME,YOUR_ACTIVEMQ2_BROKER_NAME
      - ACTIVEMQ_TRANSPORT_URI=failover:(tcp://YOUR_ACTIVEMQ1_HOSTNAME:YOUR_ACTIVEMQ1_PORT_61616,tcp://YOUR_ACTIVEMQ2_HOSTNAME:YOUR_ACTIVEMQ2_PORT_61616)?maxReconnectDelay=10000&maxReconnectAttempts=5
      - ACTIVEMQ_JMX_URL=service:jmx:rmi:///jndi/rmi://YOUR_ACTIVEMQ1_HOSTNAME:YOUR_ACTIVEMQ1_PORT_1199/jmxrmi,service:jmx:rmi:///jndi/rmi://YOUR_ACTIVEMQ2_HOSTNAME:YOUR_ACTIVEMQ2_PORT_1199/jmxrmi
```
Change the `YOUR_ACTIVEMQ_USERNAME`, `YOUR_ACTIVEMQ_PASSWORD`, `YOUR_ACTIVEMQ1_BROKER_NAME`, `YOUR_ACTIVEMQ2_BROKER_NAME`, `YOUR_ACTIVEMQ1_HOSTNAME`, `YOUR_ACTIVEMQ2_HOSTNAME`,  `YOUR_ACTIVEMQ1_PORT_1199`, `YOUR_ACTIVEMQ2_PORT_1199`, `YOUR_ACTIVEMQ1_PORT_61616` and the `YOUR_ACTIVEMQ2_PORT_61616` values using the ones specific to your ActiveMQ instances, as you previously defined these in [Step 3.2](#step-32-start-activemq-classic-jms-broker-instances) above.

> **NOTE** The configuration above is given as an example being for just for two ActiveMQ instances. The values of the `ACTIVEMQ_BROKER_NAME`, `ACTIVEMQ_TRANSPORT_URI` and the `ACTIVEMQ_JMX_URL` environment variables must reflect the details of all the ActiveMQ instances and the number of their individual parts making the full values of these environment variables must be the same:
> - For two ActiveMQ instances:
>   - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker1,domibusActiveMQBroker2
>   - ACTIVEMQ_TRANSPORT_URI=failover:(tcp://activemq1:61616,tcp://activemq2:61616)?maxReconnectDelay=10000&maxReconnectAttempts=5
>   - ACTIVEMQ_JMX_URL=service:jmx:rmi:///jndi/rmi://activemq1:1199/jmxrmi,service:jmx:rmi:///jndi/rmi://activemq2:1199/jmxrmi
> - For three ActiveMQ instances:
>   - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker1,domibusActiveMQBroker2
>   - ACTIVEMQ_TRANSPORT_URI=failover:(tcp://activemq1:61616,tcp://activemq2:61616,tcp://activemq3:61616)?maxReconnectDelay=10000&maxReconnectAttempts=5
>   - ACTIVEMQ_JMX_URL=service:jmx:rmi:///jndi/rmi://activemq1:1199/jmxrmi,service:jmx:rmi:///jndi/rmi://activemq2:1199/jmxrmi,service:jmx:rmi:///jndi/rmi://activemq3:1199/jmxrmi
> - And so on, for extra ActiveMQ instances these values must be updated to include the details of these newer instances:
>   - ACTIVEMQ_BROKER_NAME=name1,...,nameN
>   - ACTIVEMQ_TRANSPORT_URI=failover:(uri1,...,uriN)?transportOptions&nestedURIOptions
>   - ACTIVEMQ_JMX_URL=url1,...,urlN

4. shared file system:
```yaml
    volumes:
      - YOUR_TOMCAT_SHARED_FILE_SYSTEM:/data/tomcat/conf/domibus
```

Change the `YOUR_TOMCAT_SHARED_FILE_SYSTEM` value to the location of the Tomcat shared file system.
> **NOTE** All the Tomcat instances must use the same shared file system location value defined in `YOUR_TOMCAT_SHARED_FILE_SYSTEM`.

5. port mappings:
```yaml
    ports:
      - "8080:8080"
```

Optionally, you can change the port mapping to an open port that is available on the host where your Tomcat instance is running.
> **NOTE**
> We will later refer to the port where the Tomcat instances can be reached on the machine running the load balancer as `YOUR_TOMCAT1_PORT` for the first Tomcat instance, `YOUR_TOMCAT2_PORT` for the second Tomcat instance and so on.

Execute the following commands in a terminal from inside the `DOMIBUS_TOMCAT` directory on each Tomcat instance:
```shell
docker-compose up -d
```

```shell
docker-compose logs -f
```

> **NOTE** Please be patient, it can take between 2-10 minutes for each Tomcat instance to start

When you see the following line in the logs, it means that Domibus started successfully:

```shell
org.apache.catalina.startup.Catalina.start Server startup in [52508] milliseconds
```

#### Step 3.4 Load balancer

Configure your load balancer using all the hostname and port mapping values using the ones specific to your Tomcat instances, as you previously defined these in [Step 3.3](#step-33-tomcat-instances) above: `YOUR_TOMCAT1_HOSTNAME`, `YOUR_TOMCAT2_HOSTNAME`, `YOUR_TOMCAT1_PORT`, `YOUR_TOMCAT2_PORT` and so on.

> **NOTE** We will refer to the port where the load balancer can be reached from the Internet as `YOUR_PORT`.

> **NOTE** If you use a reverse proxy, you need to configure the reverse proxy to forward to the internal port


### Step 4 - Verify Domibus configuration

- Check that a folder named `domibus` is created in the `YOUR_TOMCAT_SHARED_FILE_SYSTEM` shared file system which contains the Domibus configuration files

- Please make sure your user has write permissions to the `domibus` folder created above

### Step 5 - Login into the Domibus Admin Console

- Open a browser and open the following URL:

`http://YOUR_DOMIBUS_URL:YOUR PORT/domibus`

- Login in the Domibus Admin Console with the credentials:

`admin/123456`


### Step 6 - Upload the PMode file

- Navigate to PMode->Current page in the Domibus Admin Console
- Upload the PMode file you have received from the eDelivery team

> **NOTE** For using this Access Point in a specific network, please replace the PMode file with one provided by the network

### Step 7 - Configure Domibus keystore

- Execute the following in a terminal from the DOMIBUS_TOMCAT directory:

```shell
docker-compose stop
```

- Override the existing keystore in the `YOUR_TOMCAT_SHARED_FILE_SYSTEM/domibus/keystores/gateway_keystore.jks`
  with the `gateway_keystore.jks` file you have received from the eDelivery team

- Modify the property `domibus.security.key.private.alias` from the `YOUR_TOMCAT_SHARED_FILE_SYSTEM/domibus/domibus.properties` file to set it to your party id
  Eg: `domibus.security.key.private.alias=be_test_ap`


### Step 8 - Configure Domibus truststore

- Execute the following in a terminal from the DOMIBUS_TOMCAT directory:

```shell
docker-compose start
```

```shell
docker-compose logs -f
```

- Wait for Domibus to start up
- Login into the Domibus Admin Console:

URL: `http://YOUR_DOMIBUS_URL:YOUR_PORT/domibus`

Credentials: `admin/123456`

- Navigate to `Truststores->Domibus`
- Upload the truststore you have received from the eDelivery team. The password is: `test123`
- Press the button `Reload KeyStore` in the lower right corner. You should see the message `Keystore was successfully reset`

> **NOTE** For using this Access Point in a specific network, please replace the truststore with one provided by the network

### Step 9 - Send a message to the eDelivery Domibus Access Point

- Open SoapUI
- Import the SoapUI project you have received from the eDelivery team:
    - File->Import Project
- Open the Soap UI request
- Submit request

### Step 10 - Exchange messages between webinar participants

- Exchange messages between the participants

## Docker images

You can find [here](https://code.europa.eu/edelivery/docker) the documentation for the Docker images used in this guide.

## Support and contact information

Have questions? Consult our [Q&A section]( https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus+FAQs).
Or ask your questions using [stackoverflow](http://stackoverflow.com/questions/ask).
Please use the tag `context.domibus`.

Still have questions? Contact [eDelivery support](https://ec.europa.eu/digital-building-blocks/tracker/plugins/servlet/desk/portal/6),
or by email: EC-EDELIVERY-SUPPORT@ec.europa.eu
