#!/bin/bash

echo "--------------CATALINA_HOME: ${CATALINA_HOME}"
echo "--------------DOCKER_DOMINSTALL: ${DOCKER_DOMINSTALL}"
echo "--------------DOCKER_JDBC_DRIVERS: ${DOCKER_JDBC_DRIVERS}"
echo "--------------TOMCAT_VERSION: ${TOMCAT_VERSION}"

installTomcat() {
	echo "Install Tomcat..."

	: "${TOMCAT_VERSION:?Need to set TOMCAT_VERSION non-empty}"

	local TOMCAT_ARCHIVE_LOCATION="${DOCKER_DOMINSTALL}"/tomcat
	local TOMCAT_MAIN_VERSION=$(echo "${TOMCAT_VERSION}" | cut -c1-1)
	local TOMCAT_DOWNLOAD_URL="http://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAIN_VERSION}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz"

	echo "Creating ${TOMCAT_ARCHIVE_LOCATION} directory"
	mkdir -p "${TOMCAT_ARCHIVE_LOCATION}"

	echo "   - Downloading Apache Tomcat Software Version ${TOMCAT_DOWNLOAD_URL} in ${TOMCAT_ARCHIVE_LOCATION}"

	echo "   - Downloading: ${TOMCAT_DOWNLOAD_URL} in ${TOMCAT_ARCHIVE_LOCATION}"
	echo "wget -P ${TOMCAT_ARCHIVE_LOCATION} ${TOMCAT_ARCHIVE_LOCATION} --no-check-certificate"
	wget -P "${TOMCAT_ARCHIVE_LOCATION}" "${TOMCAT_DOWNLOAD_URL}" --no-check-certificate

	echo "Creating ${CATALINA_HOME} directory"
	mkdir -p "${CATALINA_HOME}"
	echo "Installing Tomcat Version ${TOMCAT_VERSION} in ${CATALINA_HOME}"
	tar xfz "${TOMCAT_ARCHIVE_LOCATION}"/apache-tomcat-"${TOMCAT_VERSION}".tar.gz -C "${CATALINA_HOME}" --strip 1 --exclude='webapps/*' --exclude='temp'
}

installJdbcDrivers() {
	echo "Copy JDBC drivers to Tomcat libraries..."
	cp -f "${DOCKER_JDBC_DRIVERS}"/* "${CATALINA_HOME}"/lib || exit 11
}

installTomcat
installJdbcDrivers
