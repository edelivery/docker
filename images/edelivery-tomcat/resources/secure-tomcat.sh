#!/usr/bin/env bash

: "${CATALINA_HOME:?Need to set CATALINA_HOME non-empty}"

echo "--------------CATALINA_HOME: ${CATALINA_HOME}"

secureTomcat() {
	echo "Secure Tomcat..."

  sed -i.bak1 '/<\/web-app>/i \
    \<error-page\>\
        \<error-code\>404\</error-code\>\
        \<location\>\/error.jsp\</location\>\
    \</error-page\>\
    \<error-page\>\
            \<error-code\>403\</error-code\>\
            \<location\>\/error.jsp\</location\>\
    \</error-page\>\
    \<error-page\>\
            \<error-code\>500\</error-code\>\
            \<location\>\/error.jsp\</location\>\
    \</error-page\>\
    \<error-page\>\
            \<exception-type\>java.lang.Exception\</exception-type\>\
            \<location\>\/error.jsp\</location\>\
    \</error-page\>' "${CATALINA_HOME}/conf/web.xml"
  diff "${CATALINA_HOME}/conf/web.xml" "${CATALINA_HOME}/conf/web.xml.bak1"

  sed -i.bak2 '/<\/session-config>/i \
        \<cookie-config\>\
            \<secure\>true\</secure\>\
            \<http-only\>true\</http-only\>\
        \</cookie-config\>' "${CATALINA_HOME}/conf/web.xml"
  diff "${CATALINA_HOME}/conf/web.xml" "${CATALINA_HOME}/conf/web.xml.bak2"

  sed -E -i.bak 's/<Server port=".*" shutdown=".*">/<Server port="58291" shutdown="DOMIBUSSHUTDOWNCMD">/' "${CATALINA_HOME}/conf/server.xml"
  diff "${CATALINA_HOME}/conf/server.xml" "${CATALINA_HOME}/conf/server.xml.bak"

  sed -E -i.bak2 's/autoDeploy="true"/autoDeploy="false"/' "${CATALINA_HOME}/conf/server.xml"
  diff "${CATALINA_HOME}/conf/server.xml" "${CATALINA_HOME}/conf/server.xml.bak2"
}

secureTomcat

# the return code for the last diff above when finding differences is 1, making this script's usage in the Dockerfile
# fail the build, so we exit gracefully instead (we could also echo something but this is cleaner)
exit 0