# Disclaimer
Users have the responsibility to address the vulnerabilities related to this Docker image.

# Edelivery Tomcat

This image is created from edelivery-almalinux.
It contains Tomcat server prepared to deploy edelivery applications (e.g.: Domibus).

# Usage

This image is a base image. The normal usage is to extend and add specific edelivery applications.
E.g.: images/domibus-tomcat/Dockerfile

# More information

Please check the base image:
images/edelivery-almalinux