echo "Build the domibus-activemq image..."

export ACTIVEMQ_VERSION=6.0.1
export DOMIBUS_VERSION=5.1.3

docker-compose -f docker-compose.build.yml build
