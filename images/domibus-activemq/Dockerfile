FROM edelivery-almalinux-jdk17

ARG DOMIBUS_VERSION
ARG ACTIVEMQ_VERSION

ENV ACTIVEMQ_DIR=/usr/local/activemq \
    ACTIVEMQ_INSTALL_DIR=/usr/local/activemq/install \
    ACTIVEMQ_VERSION=${ACTIVEMQ_VERSION} \
    ACTIVEMQ_HOST=0.0.0.0 \
    ACTIVEMQ_JMX_ACTIVE=true \
    ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker \
    ACTIVEMQ_CONNECTOR_PORT=1199 \
    ACTIVEMQ_TRANSPORT_CONNECTOR_PORT=61616 \
    ACTIVEMQ_WEB_CONSOLE_PORT=8161 \
    ACTIVEMQ_WEB_CONSOLE_ENABLED=true \
    ACTIVEMQ_DATA=/usr/local/activemq/data \
    ACTIVEMQ_TMP=/usr/local/activemq/tmp \
    QUEUE_FS_PLUGIN_SEND=domibus.fsplugin.send.queue \
    QUEUE_WS_PLUGIN_SEND=domibus.wsplugin.send.queue \
    TZ=Europe/Brussels

USER root

COPY --chown=edelivery:edelivery resources/entrypoint.sh ${ACTIVEMQ_INSTALL_DIR}/
COPY --chown=edelivery:edelivery resources/configure-activemq.sh ${ACTIVEMQ_INSTALL_DIR}/
COPY --chown=edelivery:edelivery resources/install-activemq.sh ${ACTIVEMQ_INSTALL_DIR}/
COPY --chown=edelivery:edelivery resources/activemq.xml ${ACTIVEMQ_INSTALL_DIR}/
COPY --chown=edelivery:edelivery resources/apache-activemq-${ACTIVEMQ_VERSION}-bin.tar.gz ${ACTIVEMQ_INSTALL_DIR}/

USER edelivery

RUN "${ACTIVEMQ_INSTALL_DIR}"/install-activemq.sh
RUN "${ACTIVEMQ_INSTALL_DIR}"/configure-activemq.sh

EXPOSE "${ACTIVEMQ_CONNECTOR_PORT}" "${ACTIVEMQ_TRANSPORT_CONNECTOR_PORT}" "${ACTIVEMQ_WEB_CONSOLE_PORT}"

ENTRYPOINT ["/usr/local/activemq/install/entrypoint.sh"]
