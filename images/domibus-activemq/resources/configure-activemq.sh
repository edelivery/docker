#!/bin/bash

: "${ACTIVEMQ_DIR:?Need to set ACTIVEMQ_DIR non-empty}"

echo "--------------ACTIVEMQ_DIR: ${ACTIVEMQ_DIR}"

configureActiveMQ() {
  echo "************  Configuring ActiveMQ.."

  #fix for activemq glitch needed to make the web console visible when running from a container
  sed -i.bak 's/<property name="host" value="127.0.0.1"\/>/<property name="host" value="0.0.0.0"\/>/' "${ACTIVEMQ_INSTALL_DIR}/conf/jetty.xml"
  diff "${ACTIVEMQ_INSTALL_DIR}/conf/jetty.xml" "${ACTIVEMQ_INSTALL_DIR}/conf/jetty.xml.bak"

  #Add the SecureConnector definition
  sed -i.bak2 '/<bean id="Connector" class="org.eclipse.jetty.server.ServerConnector">/,/<\/bean -->/c \
              \<bean id="SecureConnector" class="org.eclipse.jetty.server.ServerConnector"\> \
                \<constructor-arg ref="Server" \/> \
                \<constructor-arg> \
                  \<bean id="handlers" class="org.eclipse.jetty.util.ssl.SslContextFactory$Server"> \
                    \<property name="keyStorePath" value="${activemq.conf}\/broker.ks" \/> \
                    \<property name="keyStorePassword" value="password" \/> \
                  \<\/bean> \
                \<\/constructor-arg> \
                \<property name="port" value="8161"\/> \
              \<\/bean>' "${ACTIVEMQ_INSTALL_DIR}/conf/jetty.xml"
  diff "${ACTIVEMQ_INSTALL_DIR}/conf/jetty.xml" "${ACTIVEMQ_INSTALL_DIR}/conf/jetty.xml.bak2"

  #remove simpleAuthenticationPlugin
  sed -i.bak '/<simpleAuthenticationPlugin anonymousAccessAllowed="false">/,/<\/simpleAuthenticationPlugin>/d' "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml"
  diff "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml" "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml.bak"

  #add the jaas authentication plugin
  sed -i.bak2 '/<plugins>/a \\t \
          \  <jaasAuthenticationPlugin configuration="activemq"\/\> ' "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml"
  diff "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml" "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml.bak2"

  #replace parameters with environment variables values
  sed -i "{
  					s#properties-ref=\"domibusProperties\" ##g
  					s#useJmx=\"\${activeMQ.jmx.active}#useJmx=\"\${ACTIVEMQ_JMX_ACTIVE}#g
  					s#brokerName=\"\${activeMQ.brokerName}#brokerName=\"\${ACTIVEMQ_BROKER_NAME}#g
  					s#persistent=\"\${activeMQ.persistent}#persistent=\"true#g
  					s#dataDirectory=\"\${domibus.work.location:\${domibus.config.location}}#dataDirectory=\"\${ACTIVEMQ_DATA}#g
  					s#tmpDataDirectory=\"\${domibus.work.location:\${domibus.config.location}}#tmpDataDirectory=\"\${ACTIVEMQ_TMP}#g
  					s#connectorPort=\"\${activeMQ.connectorPort}#connectorPort=\"\${ACTIVEMQ_CONNECTOR_PORT}#g
  					s#uri=\"\${activeMQ.transportConnector.uri}#uri=\"tcp:\/\/\${ACTIVEMQ_HOST}:\${ACTIVEMQ_TRANSPORT_CONNECTOR_PORT}#g
  					s#directory=\"\${domibus.work.location:\${domibus.config.location}}#directory=\"\${ACTIVEMQ_DIR}#g
  					s#physicalName=\"\${fsplugin.send.queue:domibus.fsplugin.send.queue}#physicalName=\"\${QUEUE_FS_PLUGIN_SEND}#g
  					s#physicalName=\"\${wsplugin.send.queue:domibus.wsplugin.send.queue}#physicalName=\"\${QUEUE_WS_PLUGIN_SEND}#g
  					s#queue=\"\${fsplugin.send.queue:domibus.fsplugin.send.queue}#queue=\"\${QUEUE_FS_PLUGIN_SEND}#g
            s#queue=\"\${wsplugin.send.queue:domibus.wsplugin.send.queue}#queue=\"\${QUEUE_WS_PLUGIN_SEND}#g
  	}" "${ACTIVEMQ_INSTALL_DIR}"/conf/activemq.xml

  #replace fs plugin and ws plugin queue name placeholders with corresponding environment variables in the discardingDLQBrokerPlugin definition
  sed -i.bak3 's/\${fsplugin.send.queue:domibus.fsplugin.send.queue} \${wsplugin.send.queue:domibus.wsplugin.send.queue}\" reportInterval=\"10000\"\/>/ \
\${QUEUE_FS_PLUGIN_SEND} \${QUEUE_WS_PLUGIN_SEND}\" reportInterval=\"10000\"\/>/' "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml"
  diff "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml" "${ACTIVEMQ_INSTALL_DIR}/conf/activemq.xml.bak3"
}

configureActiveMQ

# the return code for the last diff above when finding differences is 1, making this script's usage in the Dockerfile
# fail the build, so we exit gracefully instead (we could also echo something but this is cleaner)
exit 0