#!/bin/bash

: "${ACTIVEMQ_DIR:?Need to set ACTIVEMQ_DIR non-empty}"

echo "--------------ACTIVEMQ_DIR: ${ACTIVEMQ_DIR}"

installActiveMQ() {
  echo "************ Installing ActiveMQ.."

  #Extract activemq archive file to default folder were it was packed: apache-activemq-${ACTIVEMQ_VERSION}${ACTIVEMQ_VERSION}
  tar xfz "${ACTIVEMQ_INSTALL_DIR}"/apache-activemq-"${ACTIVEMQ_VERSION}"-bin.tar.gz -C "${ACTIVEMQ_INSTALL_DIR}"
  #Move contents from apache-activemq-${ACTIVEMQ_VERSION}${ACTIVEMQ_VERSION} to ${ACTIVEMQ_INSTALL_DIR}
  mv -v "${ACTIVEMQ_INSTALL_DIR}"/apache-activemq-"${ACTIVEMQ_VERSION}"/* "${ACTIVEMQ_INSTALL_DIR}"

  mv -f "${ACTIVEMQ_INSTALL_DIR}"/activemq.xml "${ACTIVEMQ_INSTALL_DIR}"/conf

  #Cleanup installation folder
  rm "${ACTIVEMQ_INSTALL_DIR}"/apache-activemq-"${ACTIVEMQ_VERSION}"-bin.tar.gz
  rm -r "${ACTIVEMQ_INSTALL_DIR}"/apache-activemq-"${ACTIVEMQ_VERSION}"
}

installActiveMQ

# the return code for the last diff above when finding differences is 1, making this script's usage in the Dockerfile
# fail the build, so we exit gracefully instead (we could also echo something but this is cleaner)
exit 0