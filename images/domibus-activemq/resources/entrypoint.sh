#!/bin/bash

echo "-------------- ACTIVEMQ_DIR=${ACTIVEMQ_DIR}"
echo "-------------- ACTIVEMQ_INSTALL_DIR=${ACTIVEMQ_INSTALL_DIR}"
echo "-------------- ACTIVEMQ_VERSION=${ACTIVEMQ_VERSION}"
echo "-------------- ACTIVEMQ_HOST=${ACTIVEMQ_HOST}"
echo "-------------- ACTIVEMQ_JMX_ACTIVE=${ACTIVEMQ_JMX_ACTIVE}"
echo "-------------- ACTIVEMQ_BROKER_NAME=${ACTIVEMQ_BROKER_NAME}"
echo "-------------- ACTIVEMQ_CONNECTOR_PORT=${ACTIVEMQ_CONNECTOR_PORT}"
echo "-------------- ACTIVEMQ_TRANSPORT_CONNECTOR_PORT=${ACTIVEMQ_TRANSPORT_CONNECTOR_PORT}"
echo "-------------- ACTIVEMQ_WEB_CONSOLE_PORT=${ACTIVEMQ_WEB_CONSOLE_PORT}"
echo "-------------- ACTIVEMQ_WEB_CONSOLE_ENABLED=${ACTIVEMQ_WEB_CONSOLE_ENABLED}"
echo "-------------- QUEUE_FS_PLUGIN_SEND=${QUEUE_FS_PLUGIN_SEND}"
echo "-------------- QUEUE_WS_PLUGIN_SEND=${QUEUE_WS_PLUGIN_SEND}"
echo "-------------- TZ=${TZ}"

#Do not overwrite existing files so that user customized files are not overwritten when restarting the containers
cp -a -n "${ACTIVEMQ_INSTALL_DIR}"/. "${ACTIVEMQ_DIR}"

#activate/deactivate the web console according to the environment variable. By default the web console is enabled
if [ "${ACTIVEMQ_WEB_CONSOLE_ENABLED}" == "true" ]
then
  sed -i.bak4 '/<context:property-placeholder/i \\t\<spring:import resource="jetty.xml"\/\>' "${ACTIVEMQ_DIR}/conf/activemq.xml"
  diff "${ACTIVEMQ_DIR}/conf/activemq.xml" "${ACTIVEMQ_DIR}/conf/activemq.xml.bak4"
fi

#set web console port as specified in the environment variable
sed -i.bak4 "s/<property name=\"port\" value=\"8161\"\/>/<property name=\"port\" value=\"${ACTIVEMQ_WEB_CONSOLE_PORT}\"\/>/g" "${ACTIVEMQ_DIR}/conf/jetty.xml"
diff "${ACTIVEMQ_DIR}/conf/jetty.xml" "${ACTIVEMQ_DIR}/conf/jetty.xml.bak4"

"${ACTIVEMQ_DIR}"/bin/activemq console
