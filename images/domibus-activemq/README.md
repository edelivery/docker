# Disclaimer
Users have the responsibility to address the vulnerabilities related to this Docker image.

# Domibus ActiveMQ

This image is created from edelivery-almalinux.
It contains a configured standalone ActiveMQ.

# Usage

Usage via docker-compose, e.g:

```
version: '3.2'
services:
  activemqc2i1:
    image: ${IMAGE_NAME_DOMIBUS_ACTIVEMQ}:${DOMIBUS_VERSION}
    environment:
       - ACTIVEMQ_DIR=/usr/local/activemq
       - ACTIVEMQ_INSTALL_DIR=/usr/local/activemq/install
       - ACTIVEMQ_VERSION=${ACTIVEMQ_VERSION}
       - ACTIVEMQ_HOST=0.0.0.0
       - ACTIVEMQ_JMX_ACTIVE=true
       - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker
       - ACTIVEMQ_CONNECTOR_PORT=1199
       - ACTIVEMQ_TRANSPORT_CONNECTOR_PORT=61616
       - ACTIVEMQ_WEB_CONSOLE_PORT=8161
       - ACTIVEMQ_WEB_CONSOLE_ENABLED=true 
       - QUEUE_FS_PLUGIN_SEND=domibus.fsplugin.send.queue
       - QUEUE_WS_PLUGIN_SEND=domibus.wsplugin.send.queue
       - TZ=Europe/Brussels
    volumes:
      - shared_file_system:/usr/local/activemq/work/kahadb
      - ./c2/conf/activemq2:/usr/local/activemq/conf
```

This image performs the setup of an ActiveMQ. It is used to run the Domibus external ActiveMQ single node or cluster.

A cluster of ActiveMQ servers can be achieved via docker-compose by configuring volumes which use the same host directory for the kahadb directory, on each ActiveMQ service. This will make the ActiveMQ nodes try to grab the same kahadb lock file, with the node which has the exclusive lock being the master and active node, and the remaining nodes acting as slaves in a failover system, in which when a node releases the lock for whatever reason it looses master status and the node which grabs the lock becomes the new master.

To configure a client to connect to an ActiveMQ cluster, the transport connector uri must use the format:

```
failover:(tcp://<activemq node1 IP>:<activemq node1 transport connector port>,tcp://<activemq nodeX IP>:<activemq nodeX transport connector port>)?<failover options>
```
where the ActiveMQ nodes must be configured using their IP as Docker internal DNS system will lose the container names when they stop and cause "unknown name" errors when the ActiveMQ client tries to resolve the name's IP.

The configuration of the service URL of the MBeanServerConnectionFactoryBean should be able to be configured with all the ActiveMQ nodes, as reported in the issue EDELIVERY-2356, the following format must be used (RMI port not used anymore):

```
service:jmx:rmi:///jndi/rmi://<activemq master node>:<activemq master node connector port>/jmxrmi"
```

For more information check:
* compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml

It is possible to enable/disable the JMX usage setting the environment variable ACTIVEMQ_JMX_ACTIVE in the corresponding ActiveMQ service section inside compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml:
- ACTIVEMQ_JMX_ACTIVE=true
By default it is enabled.

# Configure users

It is possible to configure multiple users for the ActiveMQ instances. This can be achieved by overwriting the users.properties file from the following locations(according to the container that is being configured):
 - ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (and activemq2 respectively) for C2
 - ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (and activemq2 respectively) for C3

A container restart is needed for the changes to take effect (running of the 2_startup.sh script from the 'compose' folder).
A sample file is already copied, having contents similar to the one below:

users.properties:
```
#In order to define a user add a new line in the form: username=password

admin=123456
user=changeit
```

If the ActiveMQ containers are rebuilt(the 1_buildImages.sh script is executed), the users.properties file is overwritten with the sample one
and the user needs to update it again.

# Configure queues

The user has the possibility to change queue configurations by editing or overwriting the existing activemq.xml file from one of the locations below(according to the container that is being configured):
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (and activemq2 respectively) for C2
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (and activemq2 respectively) for C3

The user can configure certain settings like memory usage, store usage, authorizations, number of redeliveries, etc, however 
the user must not change other queue definitions except FSPlugin and WSPlugin, since the changes will not be supported by Domibus.
Changing the FSPlugin and WSPlugin queue names can be made by defining the QUEUE_FS_PLUGIN_SEND and QUEUE_WS_PLUGIN_SEND environment variables in the corresponding 
ActiveMQ service section from the compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml file. The default values are:
- QUEUE_FS_PLUGIN_SEND=domibus.fsplugin.send.queue
- QUEUE_WS_PLUGIN_SEND=domibus.wsplugin.send.queue

# Web console
In order to monitor the ActiveMq instance, a web console is available on https://HostIP:ACTIVEMQ_WEB_CONSOLE_PORT.
The default port is 8161, and it can be configured in compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml, in the section of the corresponding ActiveMQ service:
- ACTIVEMQ_WEB_CONSOLE_PORT=8161

Please be aware that the connection to the web console is SSL secured and therefore the URL begins with https. 
The secured connection can be configured in the following section of jetty.xml:
```
<bean id="SecureConnector" class="org.eclipse.jetty.server.ssl.SslSelectChannelConnector">
    <property name="port" value="8161" />
    <property name="keystore" value="file:${activemq.conf}/broker.ks" />
    <property name="password" value="password" />
</bean>
```
jetty.xml can be edited or overwritten, and it is located in one of the following folders(please configure the one corresponding to the corresponding ActiveMQ instance):
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (or activemq2 respectively) for C2
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (or activemq2 respectively) for C3

The default keystore file is broker.ks, located in the folders above, it can be replaced/renamed/edited and configuration must be updated accordingly.  
Also it can be replaced by a .jks file.

The web console users are defined in the jetty-realm.properties file, which can be customized by updating or overwriting the file in the corresponding container location below(according to the container that is being configured):
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c2/conf/activemq1 (or activemq2 respectively) for C2 
- ${project_folder}/compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/c3/conf/activemq1 (or activemq2 respectively) for C3

A sample jetty-realm.properties is shown below:
```
# Defines users that can access the web (console, demo, etc.)
# username: password [,rolename ...]
admin: admin, admin
user: user, user
```

It is possible to enable/disable the web console by setting the environment variable ACTIVEMQ_WEB_CONSOLE_ENABLED in corresponding ActiveMQ service section inside compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml:
- ACTIVEMQ_WEB_CONSOLE_ENABLED=true
By default it is enabled.
