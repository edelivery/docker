# Disclaimer
Users have the responsibility to address the vulnerabilities related to this Docker image.

# Domibus Tomcat

This image is created from edelivery-tomcat:9.0.85(for the time being). It contains the Domibus application deployed and configured on Tomcat.

# Usage
## Environment Properties

| Name	                              | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |                          Required                           |                                       Default |
|------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------:|----------------------------------------------:|
| ACTIVEMQ_EXTERNAL_BROKER_TYPE      | Defines whether Domibus should start start using the default embedded ActiveMQ broker - if left empty - or using an external ActiveMQ broker or cluster - when passing a value in -. If non-empty, Domibus will pass an empty value for the activeMQ.embedded.configurationFile system property when starting, forcing the use of an external ActiveMQ broker or cluster. <br/><br/>Possible values:<br/> - empty: no external brokers, use the embedded ActiveMQ broker provided by Domibus; <br/> - single: external single broker instance; <br/> - cluster: an external cluster of exactly 2 ActiveMQ independent broker instances; <br/>  master-slave: an external cluster of ActiveMQ broker instances running in master-slave mode using a shared files system. |                                                             |                                               |
| ACTIVEMQ_BROKER_NAME               | The name of the ActiveMQ embedded broker or the names of all the external broker names as a comma-separated value, whether independent or part of a cluster (e.g. nodes in a master-slave setup); this number of broker names must match the number of ActiveMQ JMX service URLs                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                                                             |                         domibusActiveMQBroker |
| ACTIVEMQ_CONNECTOR_PORT            | The port number for the JMX connector of the ActiveMQ embedded broker; ignored when using an external broker or cluster of brokers                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                                             |                                          1199 |
| ACTIVEMQ_HOST                      | The hostname or the IP address of the ActiveMQ embedded broker or of all the external brokers as a comma-separated value, whether independent or part of a cluster (e.g. nodes in a master-slave setup); used to build the transport connector URI and the JMX service URLs in all external broker setups excepting master-slave                                                                                                                                                                                                                                                                                                                                                                                                                                        |                                                             |                                       0.0.0.0 |
| ACTIVEMQ_USERNAME                  | The name of the user of the ActiveMQ embedded broker or for any external brokers, whether independent or part of a cluster                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |                                                             |                                       domibus |
| ACTIVEMQ_PASSWORD                  | The password of the ActiveMQ embedded broker or for any external brokers, whether independent or part of a cluster                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                                             |                                      changeit |
| ACTIVEMQ_TRANSPORT_CONNECTOR_PORT  | The port number for the transport connector URI of the ActiveMQ embedded broker; used to build the transport connector URI in all external broker setups excepting master-slave                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |                                                             |                                         61616 |
| ACTIVEMQ_TRANSPORT_URI             | In an external ActiveMQ master-slave cluster setup, the transport connector URI involving all the external brokers as a comma-separated value. Can be used for defining the failover in case of multiple broker instances inside a cluster.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Mandatory if ACTIVEMQ_EXTERNAL_BROKER_TYPE = "master-slave" |                                               |
| ACTIVEMQ_JMX_URL                   | In an external ActiveMQ master-slave cluster setup, the JMX service URLs of all the external brokers as a comma-separated value; this number of ActiveMQ JMX service URLs must match the number of broker names (check ACTIVEMQ_BROKER_NAME)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Mandatory if ACTIVEMQ_EXTERNAL_BROKER_TYPE = "master-slave" |                                               |
| CHECK_DEFAULT_PASSWD               | Whether Domibus should fail to start when any of its users use the default password (i.e. "123456") or not; If non-empty, Domibus will pass this value to the domibus.passwordPolicy.checkDefaultPassword system property when starting                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                                             |                                               |
| DOMAINS_COUNT                      | Number of domains when Domibus is running in a multitenancy setup:<br/> - DOMAINS_COUNT = 1 => single tenancy;<br/> - DOMAINS_COUNT > 1 => multitenancy                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                          Mandatory                          |                                             1 |
| DB_TYPE                            | The type of the database that Domibus will connect to and use; <br/><br/>Possible values:<br/> - MySQL: Domibus will connect to a MySQL database;<br/> - Oracle: Domibus will connect to an Oracle database                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |                          Mandatory                          |                                         MySQL |
| DB_HOST                            | The hostname or the IP address of the database where Domibus will connect to and use                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |                                                             |                                               |
| DB_NAME                            | The database name when Domibus will connect to a MySQL database or the service name when Domibus will connect to an Oracle database; in multitenancy MySQL, the DB_NAME is updated with the DOMIBUS_GENERAL_SCHEMA value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |                          Mandatory                          |                                       domibus |
| DB_PORT                            | The port number which Domibus will use to connect                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |                          Mandatory                          |                                               |
| DB_USER                            | The name of the MySQL database user or the Oracle user schema which Domibus will use to connect                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |                          Mandatory                          |                                               |
| DB_PASS                            | The password which Domibus will use to connect                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |                          Mandatory                          |                                               |
| DOMIBUS_GENERAL_SCHEMA             | The general schema in case Domibus is running in multitenancy                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |                       Mandatory in MT                       |                               domibus_general |
| DOMIBUS_DOMAIN_SCHEMA_PREFIX       | The domain schema prefix in case Domibus is running in multitenancy                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                       Mandatory in MT                       |                                domibus_domain |
| DEFAULT_USER_AUTOGENERATE_PASSWORD | Whether Domibus should automatically generate the passwords for the default users (i.e. the super user in MT and the admin user in ST)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |                                                             |                                         false |
| DOMAIN_ALIASES                     | The domain aliases when Domibus is running in multitenancy as a comma-separated value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |                       Mandatory in MT                       |                                       blue_gw |
| DOMIBUS_INIT_PROPERTY_DELIMITER    | Delimiter when passing in more than one extra Domibus property                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |                                                             |                                  &#124;&#124; | 
| DOMIBUS_INIT_PROPERTIES            | Extra Domibus properties; multiple properties can be passed in using the value of the DOMIBUS_INIT_PROPERTY_DELIMITER environment property as a delimiter                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |                                                             |                                               |
| DOMIBUS_CONFIG_PROPERTY_FILE       | Whether to replace extra Domibus properties in the domibus.properties file when true or to use them when Domibus starts as additional system properties otherwise (i.e. -Ddomibus.system.property=value ...)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |                                                             |                                         false |
| domibus...                         | Extra Domibus properties to use when Domibus starts as additional system properties (i.e. -Ddomibus.system.property=value ...). <br/><br/>For example:<br/> - global properties: domibus.deployment.clustered=true;<br/> - domain properties (also for the default domain): domain_name.domibus.security.key.private.alias=private_key_alias.                                                                                                                                                                                                                                                                                                                                                                                                                           |                                                             |                                               |
| DOMIBUS_VERSION                    | The version of the Domibus instance                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                                                             |                                  5.2-SNAPSHOT |
| LOGGER_LEVEL_EU_DOMIBUS            | The level of the eu.domibus logger                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                                             |                                         DEBUG |
| LOGGER_LEVEL_ORG_APACHE_CXF        | The level of the org.apache.cxf logger                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |                                                             |                                          INFO |
| MEMORY_SETTING_MIN                 | The minimum memory allocated to the Domibus instance                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |                                                             |                                      -Xms128m |
| MEMORY_SETTING_MAX                 | The maximum memory allocated to the Domibus instance                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |                                                             |                                     -Xmx1024m |
| MEMORY_SETTINGS                    | The memory settings allocated to the Domibus instance; when manually set, it ignores any MEMORY_SETTING_MIN and MEMORY_SETTING_MAX previously set                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |                                                             | "${MEMORY_SETTING_MIN} ${MEMORY_SETTING_MAX}" |


The following environment properties should not be normally changed or will have no effect if done:

| Name	                        | Description                                                                                                                                                                           |                                                                                                                                                                      Default |
|------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| PRODUCTION_MODE              | Whether this Tomcat instance is running in production mode, when its value is true; otherwise, when false, this Tomcat instance is running in development mode.                       |                                                                                                                                                                              |
| DOMIBUS_CONFIG_LOCATION      | Domibus configuration location                                                                                                                                                        |                                                                                                                                                    /data/tomcat/conf/domibus |
| TZ                           | The timezone of the system where Domibus will be running                                                                                                                              |                                                                                                                                                              Europe/Brussels |
| ENABLE_JACOCO                | Whether to enable the JaCoCo (Java Code Coverage) tool when set to true or otherwise, disable it                                                                                      |                                                                                                                                                                              |
| MAXIMUM_WAIT_TIME_IN_SECONDS | The maximum total time to wait in seconds for a particular resource to become available (e.g. database, ActiveMQ broker etc.)                                                         |                                                                                                                                                                          900 |
| SLEEP_TIME_IN_SECONDS        | The number of seconds to sleep before checking again if a particular resource has become available                                                                                    |                                                                                                                                                                           10 |
| SERVER_DEBUG                 | Whether to allow connecting a remote Java debugger to Domibus when set to true or otherwise not                                                                                       |                                                                                                                                                                              |
| JDK11_RUNTIME                | Specifies that Domibus is running using a Java 11 runtime when true or otherwise, using a Java 8 runtime; used to build the correct JDWP agent library line when SERVER_DEBUG is true |                                                                                                                                                                              |
| CATALINA_HOME                | Tomcat home directory                                                                                                                                                                 |                                                                                                                                                                 /data/tomcat |
| CATALINA_OPTS                | It should always include the value for the domibus.config.location system property                                                                                                    |                                                                                                                                                                              |
| DOCKER_DATA                  | Domibus data directory                                                                                                                                                                |                                                                                                                                                                        /data |
| DOCKER_DOMIBUS_DISTRIBUTION  | Domibus distribution directory                                                                                                                                                        |                                                                                                                                                     /data/domInstall/domibus |
| DOCKER_DOMINSTALL            | Domibus installation directory                                                                                                                                                        |                                                                                                                                                             /data/domInstall |
| DOCKER_JACOCO                | Jacoco home directory                                                                                                                                                                 |                                                                                                                                                                 /data/jacoco |
| DOCKER_JDBC_DRIVERS          | JDBC drivers location                                                                                                                                                                 |                                                                                                                                                 /data/domInstall/jdbcDrivers |
| DOCKER_SCRIPTS               | Domibus scripts directory                                                                                                                                                             |                                                                                                                                                     /data/domInstall/scripts |
| DOCKER_TIME_DIR              | System time directory                                                                                                                                                                 |                                                                                                                                                              /usr/local/lib/ |
| JAVA_HOME                    | Java home directory                                                                                                                                                                   |                                                                                                                                               /usr/local/java/jdk-11.0.9.1+1 |
| SQLPLUS_HOME                 | SQLPlus home directory                                                                                                                                                                |                                                                                                                                                    /usr/local/Oracle/SQLPlus |
| SQLPLUS                      | SQLPlus executable                                                                                                                                                                    |                                                                                                                                            /usr/local/Oracle/SQLPlus/sqlplus |
| LD_LIBRARY_PATH              | Shared libraries directory                                                                                                                                                            |                                                                                                                                                    /usr/local/Oracle/SQLPlus |
| PATH                         | Path directories                                                                                                                                                                      | /usr/local/java/jdk-11.0.9.1+1/bin:/data/tomcat/bin:/data/domInstall/scripts:/usr/local/java/jdk-11.0.9.1+1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin |
| DOCKERIZE_VERSION            | Dockerize version                                                                                                                                                                     |                                                                                                                                                                       v0.5.0 |
| TOMCAT_VERSION               | Tomcat version                                                                                                                                                                        |                                                                                                                                                                       9.0.85 |
| MYSQL_JDBC_DRIVER_NAME       | The name of the JDBC driver to use when connecting to a MySQL database                                                                                                                |                                                                                                                                          mysql-connector-java-8.0.19-bin.jar |
| ORACLE_JDBC_DRIVER_NAME      | The name of the JDBC driver to use when connecting to an Oracle database                                                                                                              |                                                                                                                                                          ojdbc8-21.1.0.0.jar |

## Single Tenancy

* DOMAINS_COUNT = 1 (default)
* DOMAIN_ALIASES = blue_gw (default)
* no need to specify DOMIBUS_GENERAL_SCHEMA
* no need to specify DOMIBUS_DOMAIN_SCHEMA_PREFIX 

Usage via docker-compose, e.g:

```
version: '3.2'
services:
  tomcatc2:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      environment:
         - DB_TYPE=MySQL
         - DB_HOST=mysqlc2
         - DB_PORT=3306
         - DB_USER=edelivery
         - DB_PASS=edelivery
         - DOMAINS_COUNT=1
         - DOMAIN_ALIASES=blue_gw
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus
      depends_on:
        - mysqlc2
```

For more information check:
* compose/test-tomcat9-wildfly26-mysql8/docker-compose.yml

## Multitenancy

* DOMAINS_COUNT > 1
* DOMIBUS_GENERAL_SCHEMA = domibus_general (default)
* DOMIBUS_DOMAIN_SCHEMA_PREFIX = domibus_domain (default) 
* DOMAIN_ALIASES=blue_gw,green_gw 
(comma-delimited aliases matching the number of domains; 
must be the same for each of the servers in a cluster)
* no need to specify DB_NAME

Usage via docker-compose, e.g:
```
version: '3.2'
services:
  tomcatc2:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9_JDK_8}:${DOMIBUS_VERSION}
      environment:
         - DB_TYPE=MySQL
         - DB_HOST=mysqlc2
         - DB_PORT=3306
         - DB_USER=edelivery
         - DB_PASS=edelivery
         - DOMAINS_COUNT=2
         - DOMAIN_ALIASES=blue_gw,green_gw
         - CHECK_DEFAULT_PASSWD=false
         - DOMIBUS_GENERAL_SCHEMA=domibus_general
         - DOMIBUS_DOMAIN_SCHEMA_PREFIX=domibus_domain
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus
      depends_on:
        - mysqlc2
```

This image performs the setup and deploy of Domibus on Tomcat server. It is used to run the Domibus application single node or cluster.

For more information check:
* compose/test-tomcat9-wildfly26-mysql8-multi/docker-compose.yml

## Cluster

To configure a cluster of Tomcat instances, add multiple services and configure each of them using a different TOMCAT_NODE environment variable (e.q. 01, 02, ...). The instances MUST share
the same Domibus configuration via a docker bind mount (when the user wants to have easy access on the host machine to the Domibus configuration) or via a docker volume:
* _docker volume_ (not recommended because the docker volume get created in the docker volume area which is not easy accessible): each Tomcat instance should define a volume section (what precedes the ':' character') that references an entry from the global parent volumes section and which maps to each of the Domibus config location paths from inside the containers (what follows the ':' character):
```
version: '3.2'
services:
    tomcatc2i1:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      ...
      volumes:
          - shared-domibus-conf:/data/tomcat/conf/domibus

    tomcatc2i1:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      ...
      volumes:
          - shared-domibus-conf:/data/tomcat/conf/domibus
...
volumes:
    - shared-domibus-conf
```
* _bind mount_: each Tomcat instance should define a volume section that maps the Domibus config location path from inside the container (what follows the ':' character) to an absolute or relative path on the host machine (what precedes the ':' character'):
```
version: '3.2'
services:
    tomcatc2i1:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      ...
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus

    tomcatc2i1:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      ...
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus
```

The cluster can be configured against a network of external ActiveMQ broker based on the domibus-tomee image.

Usage via docker-compose, e.g:
```
version: '3.2'
services:
    tomcatc2i1:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      environment:
          - TOMCAT_NODE=01
          - DB_TYPE=MySQL
          - DB_HOST=mysqlc2
          - DB_PORT=3306
          - DB_USER=edelivery
          - DB_PASS=edelivery
          - ACTIVEMQ_EXTERNAL_BROKER_TYPE=master-slave
          - ACTIVEMQ_USERNAME=domibus
          - ACTIVEMQ_PASSWORD=changeit
          - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker1,domibusActiveMQBroker2
          - ACTIVEMQ_TRANSPORT_URI=failover:(tcp://activemqc2i1:61616,tcp://activemqc2i2:61616)?maxReconnectDelay=10000&maxReconnectAttempts=5
          - ACTIVEMQ_JMX_URL=service:jmx:rmi:///jndi/rmi://activemqc2i1:1199/jmxrmi,service:jmx:rmi:///jndi/rmi://activemqc2i2:1199/jmxrmi
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus
      command: dockerize -wait tcp:/mysqlc2:3306 -timeout 20s
      depends_on:
        - mysqlc2
        - activemqc2i1
        - activemqc2i2
        
    tomcatc2i2:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
      environment:
          - TOMCAT_NODE=02
          - DB_TYPE=MySQL
          - DB_HOST=mysqlc2
          - DB_PORT=3306
          - DB_USER=edelivery
          - DB_PASS=edelivery
          - ACTIVEMQ_EXTERNAL_BROKER_TYPE=master-slave
          - ACTIVEMQ_USERNAME=domibus
          - ACTIVEMQ_PASSWORD=changeit
          - ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker1,domibusActiveMQBroker2
          - ACTIVEMQ_TRANSPORT_URI=failover:(tcp://activemqc2i1:61616,tcp://activemqc2i2:61616)?maxReconnectDelay=10000&maxReconnectAttempts=5
          - ACTIVEMQ_JMX_URL=service:jmx:rmi:///jndi/rmi://activemqc2i1:1199/jmxrmi,service:jmx:rmi:///jndi/rmi://activemqc2i2:1199/jmxrmi
      volumes:
        - ./c2/conf/domibus:/data/tomcat/conf/domibus
      command: dockerize -wait tcp:/mysqlc2:3306 -timeout 20s
      depends_on:
        - mysqlc2
        - activemqc2i1
        - activemqc2i2
     
    httpdc2:
      depends_on:
        - tomcatc2i1
        - tomcatc2i2
      image: ${IMAGE_NAME_EDELIVERY_HTTPD}:2.4.58
      environment:
        - VHOST_CORNER_HOSTNAME=edelivery.domibus.eu
        - NODES_COUNT=2
        - NODE_HOSTNAMES=tomcatc2i1,tomcatc2i2
        - NODE_PORT_NUMBERS=8080,8080
```

For more information check:
* compose/test-tomcat9-wildfly26-mysql8-jdk8-cluster/docker-compose.yml

## Oracle Java 8 and Open JDK 11

By default, the domibus-tomcat9 image is configured to run Domibus using Open JDK 11. In order to run Domibus using Oracle Java 8, you need to use the domibus-tomcat9-jdk8 image as below:
```
version: '3.2'
services:
    tomcatc2i1:
      image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9_JDK_8}:${DOMIBUS_VERSION}
...
```

## Database

**Note:** For production purposes, we recommend the domibus-tomcat9 image to use an external database (non-docker).

The domibus-tomcat9 image supports connecting to Oracle and MySQL databases.

By default, the domibus-tomcat9 image is configured to run Domibus configured with a MySQL database. In order to run Domibus configured with an Oracle database, you need to use something similar to the following:
```
version: '3.2'
services:
    tomcatc2i1:
    image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
    environment:
       - DB_TYPE=Oracle
       - DB_NAME=ORCLPDB1
       - DB_HOST=oracledbc2
       - DB_PORT=1521
       - DB_USER=domibus
       - DB_PASS=domibus
...
```

## FS-Plugin

**Note** On Windows, when bind mounting to a path inside a Linux container that has been created with Docker Desktop 
using the WSL integration, any file that gets transferred from the host into the container gets created using the root 
user. This creates issues when using a bind mount for the FS-plugin location directly from the Windows host. In order to
avoid this, please make sure you create the container and its bind mount from WSL instead:
```
version: '3.2'
services:
    tomcatc2i1:
    image: ${IMAGE_TAG:-domibustest}/${IMAGE_NAME_DOMIBUS_TOMCAT_9}:${DOMIBUS_VERSION}
    environment:
       - DB_TYPE=Oracle
       - DB_NAME=ORCLPDB1
       - DB_HOST=oracledbc2
       - DB_PORT=1521
       - DB_USER=domibus
       - DB_PASS=domibus
    volumes:
# Wrong way to use a bind mount for the FS-Plugin from Windows
#        - C:/Users/<MY_WINDOWS_USER>/fs_plugin_data:/data/tomcat/fs_plugin_data/MAIN
# Correct way to use a bind mount for the FS-Plugin from  WSL
        - /home/<MY_WSL_USER>/fs_plugin_data:/data/tomcat/fs_plugin_data/MAIN
...
```

Please also check the documentation about file sharing on Windows for Docker:
- [https://docs.docker.com/desktop/settings/windows/#file-sharing](https://docs.docker.com/desktop/settings/windows/#file-sharing)
- [https://docs.docker.com/desktop/troubleshoot/topics/#topics-for-windows](https://docs.docker.com/desktop/troubleshoot/topics/#topics-for-windows)


## Container shell access

To access the container please run the following command:
```
$ docker exec -it tomcatc2 bash
```

# More information

Please check the base image: images/edelivery-tomcat
