ARG TOMCAT_VERSION="9.0.85"

FROM edelivery-tomcat:${TOMCAT_VERSION}

#
# Build arguments, which are environment variables accessible only during the build process.
#
ARG DOMIBUS_VERSION
ARG TOMCAT_VERSION

#
# Environment variables required for the project setup
#
ENV DOMIBUS_VERSION="${DOMIBUS_VERSION}" \
    TOMCAT_VERSION="${TOMCAT_VERSION}" \
    DOMAINS_COUNT=1 \
    DOMAIN_ALIASES=blue_gw \
    DOMIBUS_GENERAL_SCHEMA=domibus_general \
    DOMIBUS_DOMAIN_SCHEMA_PREFIX=domibus_domain \
    LOGGER_LEVEL_EU_DOMIBUS=INFO \
    LOGGER_LEVEL_ORG_APACHE_CXF=WARN \
    DB_TYPE="" DB_HOST="" DB_PORT="" DB_NAME="domibus" DB_USER="" DB_PASS="" \
    DOMIBUS_CONFIG_PROPERTY_FILE=false \
    ACTIVEMQ_HOST=0.0.0.0 \
    ACTIVEMQ_BROKER_NAME=domibusActiveMQBroker \
    ACTIVEMQ_CONNECTOR_PORT=1199 \
    ACTIVEMQ_TRANSPORT_CONNECTOR_PORT=61616 \
    ACTIVEMQ_USERNAME=domibus \
    ACTIVEMQ_PASSWORD=changeit \
    DOMIBUS_CONFIG_LOCATION="${CATALINA_HOME}"/conf/domibus \
    ENABLE_JACOCO="${ENABLE_JACOCO}" \
    DEFAULT_USER_AUTOGENERATE_PASSWORD=false

ENV CATALINA_OPTS="-Ddomibus.config.location=${DOMIBUS_CONFIG_LOCATION}"

# Create the Domibus configuration directory using the 'edelivery' user as owner
RUN mkdir -p "${DOMIBUS_CONFIG_LOCATION}" \
    && chown -R edelivery "${DOMIBUS_CONFIG_LOCATION}"

#
# Add required resources
#
USER edelivery
COPY --chown=edelivery:edelivery resources/domibus-msh-distribution-"${DOMIBUS_VERSION}"-*.zip "${DOCKER_DOMIBUS_DISTRIBUTION}"/
COPY --chown=edelivery:edelivery resources/server_clustered.xml "${CATALINA_HOME}"/conf
COPY --chown=edelivery:edelivery resources/entrypoint.sh "${DOCKER_SCRIPTS}"
COPY --chown=edelivery:edelivery resources/*.functions "${DOCKER_SCRIPTS}"/

RUN chmod +x "${DOCKER_SCRIPTS}"/*

WORKDIR "${CATALINA_HOME}"

# Expose ports: Connector / Transport Connector
EXPOSE "${ACTIVEMQ_CONNECTOR_PORT}" "${ACTIVEMQ_TRANSPORT_CONNECTOR_PORT}"

#
# Execute Tomcat service using the 'edelivery' user
#
ENTRYPOINT ["entrypoint.sh"]
