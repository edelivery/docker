# Disclaimer
Users have the responsibility to address the vulnerabilities related to this Docker image.

# eDelivery Almalinux

This image is created from Almalinux
It contains the Domibus application deployed and configured on Wildfly.

# Usage

This image is a base image. The normal usage is to extend and add specific eDelivery tools or applications.
E.g.: images/edelivery-almalinux/Dockerfile

# More information

Please check the base image:
https://hub.docker.com/_/almalinux